/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function getUserDetails(){
	let userFullName = prompt("Please enter your full name:");
	let userAge = prompt("Please enter your age:");
	let userLocation = prompt("Please enter your location:");

	console.log("Hello, " + userFullName + "!");
	console.log("You are "+ userAge +" Years Old.");
	console.log("You live in "+ userLocation +".");
}

getUserDetails();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	
function favoriteBands(){
	let band1 = "Silk Sonic";
	let band2 = "Big Time Rush";
	let band3 = "One Direction";
	let band4 = "Black Pink";
	let band5 = "Paramore";

	console.log('1. ' + band1);
	console.log('2. ' + band2);
	console.log('3. ' + band3);
	console.log('4. ' + band4);
	console.log('5. ' + band5);
}
favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function favoriteMovies(){
	const rottenTomatoesHeader = "Rotten Tomatoes Rating: ";
	let movie1Title = "The Menu";
	let movie1Score = "88%";

	let movie2Title = "The Leftovers";
	let movie2Score = "91%";

	let movie3Title = "Nightcrawler";
	let movie3Score = "95%";

	let movie4Title = "3 Idiots";
	let movie4Score = "100%";

	let movie5Title = "Parasite";
	let movie5Score = "99%";

	console.log('1. ' + movie1Title);
	console.log(rottenTomatoesHeader + movie1Score);

	console.log('2. ' + movie2Title);
	console.log(rottenTomatoesHeader + movie2Score);

	console.log('3. ' + movie3Title);
	console.log(rottenTomatoesHeader + movie3Score);

	console.log('4. ' + movie4Title);
	console.log(rottenTomatoesHeader + movie4Score);

	console.log('5. ' + movie5Title);
	console.log(rottenTomatoesHeader + movie5Score);
}

favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

/*printUsers();*/
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
/*console.log(friend1);
console.log(friend2);*/
